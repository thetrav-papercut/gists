package main

import (
	"fmt"
	"testing"

	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/dgrijalva/jwt-go"
)

const jwtString = "redacted"
const secret = "redacted"

type CustomClaims struct {
	CanWork bool `json:"CAN_WORK"`
	jwt.StandardClaims
}

func TestJoseValidation(t *testing.T) {
	j, err := jws.ParseJWT([]byte(jwtString))
	if err != nil {
		t.Fatalf("JWT token parse error: %v", err)
	}
	// TODO: restore verification when BEx tell me where to get the key from
	err = j.Verify([]byte(secret), crypto.SigningMethodHS256)
	if err != nil {
		t.Fatalf("JWT validation error %v", err)
	}
}
func TestDgrijalvaValidation(t *testing.T) {
	token, err := jwt.ParseWithClaims(jwtString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(secret), nil
	})
	if err != nil {
		t.Fatalf("erorr parsing %v", err)
	}
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		if !claims.CanWork {
			t.Fatalf("incorrect parsing %v", claims)
		}
	} else {
		t.Fatalf("error %v", err)
	}
}

